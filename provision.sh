#! /bin/bash

set -ex

yum -y install tar make gcc-c++

useradd memcached

cd /usr/local/src
curl -L  https://github.com/libevent/libevent/releases/download/release-2.0.22-stable/libevent-2.0.22-stable.tar.gz -o libevent-2.0.22-stable.tar.gz
tar zxvf libevent-2.0.22-stable.tar.gz
cd libevent-2.0.22-stable
./configure && make && make install

cd /usr/local/src
curl -L http://www.memcached.org/files/memcached-1.4.25.tar.gz -o memcached-1.4.25.tar.gz
tar zxvf memcached-1.4.25.tar.gz
cd memcached-1.4.25
./configure && make && make install
